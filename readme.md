# FUKA
A Dribbble Client.

> Still in devlopment
>
> Demo: http://fuka.luish.com.br:8080/

## Some tools
- [Star](https://github.com/beeblebrox3/star)
- [React](https://facebook.github.io/react/)
- [Webpack](https://webpack.js.org/)
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)
- [PHP](http://www.php.net/)
- [Dribbble API](http://developer.dribbble.com/v1/)
- [Less](http://lesscss.org/)

## Requirements
To run this project on your machine, you need [docker](https://www.docker.com/) and [docker-compose](https://docs.docker.com/compose/).
If you don't have and don't want to install, you will need at least [nodejs](https://nodejs.org/en/) and [php](http://www.php.net/). It will be easier if you just get docker ;)

## Instructions
### Using docker
On docker compose config file we map two ports: **82** and **8080**. If you're using those, you can edit the file and will have to change  the `.env.js` (it will be created automatically when you start the development server).

#### Development
```shell
docker-compose -f docker-compose-dev.yml up
// assuming that you already cloned the repo and are on the root directory ;)
```
A lot of things will happen. Wait until webpack finishes packing and open your browser at http://localhost:8080.

#### Production*
If the production `.env.js` must be different, you have to create a `.env.production.js`. Or don't, the `.env.js` will be used as fallback.

```shell
docker-compose up
```

It will pack the js code, compile less and start a nginx container to serve the files. No webpack-dev-server stuff.

Wait until webpack finishes packing and open your browser on http://localhost:8080
