// @flow

// 3rd party
import React from "react";
import ReactDOM from "react-dom";

import debounce from "debounce";
import Superagent from "superagent";
import Page from "page";
import qs from "qs";
import md5 from "md5";

// core
import ServicesContainer from "./core/ServicesContainer";
import EventManager from "./core/EventManager";
import App from "app";
import Config from "./config";

// Register
App.libs.React = React;
App.libs.ReactDOM = ReactDOM;
App.libs.debounce = debounce;
App.libs.Superagent = Superagent;
App.libs.Page = Page;
App.libs.qs = qs;
App.libs.md5 = md5;

App.Config = Config;

App.ServicesContainer = new ServicesContainer();
App.ServicesContainer.define("EventManager", EventManager);
// shortcuts
App.service = (service) => App.ServicesContainer.get(service);
App.provider = (service) => App.ServicesContainer.get(service);

// shortcut
App.EventManager = App.ServicesContainer.get("EventManager");

require("./helpers/index");
require("./services/index");
require("./providers/index");
require("./components/index");
require("./routes/index");

require("../css/main.less");

export default App;
