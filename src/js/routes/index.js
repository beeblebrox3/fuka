import App from "app";
// import qs from "qs";

const Page = App.libs.Page;
const RDOM = App.libs.ReactDOM;
const React = App.libs.React;

let Router = {};
const DOMNode = document.querySelector("#react-root");
// const parseqs = (context) => context.qs.parse()

Router.setRoute = function (route) {
    "use strict";

    Page.redirect(route);
};

Router.renderPage = function (Page, context) {
    "use strict";

    RDOM.render(
        <Page { ...context.params } { ...context.query } context={ context }/>,
        DOMNode
    );
};

Router.renderPageWithLayout = function (Layout, Page, context) {
    "use strict";

    RDOM.render(
        <Layout>
            <Page { ...context.params } { ...context.query } context={ context }/>
        </Layout>,
        DOMNode
    );
};

Router.getCurrentPath = () => Page.current;

App.ServicesContainer.setInstance("ROUTER", Router);
require("./middlewares");
require("./applicationRoutes");

Page("*", Router.renderPageWithLayout.bind(null, App.components.Application, App.components.NotFound));

Page.start({
    click: true,
    hashbang: false
});
