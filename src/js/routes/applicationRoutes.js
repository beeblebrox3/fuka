import App from "app";

const Page = App.libs.Page;

const Router = App.ServicesContainer.get("ROUTER");
const defaultLayout = App.components.Application;
const Pages = App.components.pages;

/**
 * @var {DribbbleProvider}
 */
const DribbbleProvider = App.ServicesContainer.get("DribbbleProvider");

// Page("/", Router.renderPageWithLayout.bind(null, defaultLayout, Pages.Home));
Page("/", context => {
    document.title = "Fuka - A Dribbble Client";
    Router.renderPageWithLayout(defaultLayout, Pages.Home, context);
});

// testomg route
Page("/logging", context => {
    document.title = "Fuka - A Dribbble Client";
    Router.renderPageWithLayout(defaultLayout, Pages.Working, context);
});

Page("/shots/:shotId", context => {
    document.title = "Shot... | Fuka";
    Router.renderPageWithLayout(defaultLayout, Pages.Shot, context);
});

Page("/auth", (context, next) => {
    document.title = "Authenticating... | Fuka";

    // without the code, let the request go (probably 404)
    if (!context.query.hasOwnProperty("code") || !context.query.code.length) {
        debugger
        console.log("no code")
        next();
        return;
    }

    DribbbleProvider.auth(context.query.code).then(() => {
        console.log("deu bom!");
        Page("/");
    }).catch(err => {
        console.log("deu ruim!");
        console.log(err);
    });

    Router.renderPageWithLayout(defaultLayout, Pages.Working, context);
});
