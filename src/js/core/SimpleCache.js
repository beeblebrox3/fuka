function isExpired(storedObject) {
    const now = new Date().getTime();

    return (now - storedObject.created_at) / 1000 > storedObject.ttl;
}

function SimpleCache(key) {
    this.buckets = {
        default: {}
    };
    this.currentBucket = this.buckets.default;
    this.key = key || "star_SimpleCache";
    this.autosave = true;
}

SimpleCache.prototype._save = function () {
    localStorage.setItem(this.key, JSON.stringify(this.buckets));
};

SimpleCache.prototype._load = function () {
    try {
        let data = localStorage.getItem(this.key);
        this.buckets = JSON.parse(data);
        if (!this.buckets.hasOwnProperty("default")) {
            this.buckets.default = {};
        }
        this.currentBucket = this.buckets.default;
    } catch (e) {
        console.log("failed to load data from local storage.");
    }
};

SimpleCache.prototype.setAutosave = function (value) {
    this.autosave = value;
};

SimpleCache.prototype.has = function (key) {
    return this.currentBucket.hasOwnProperty(key);
};

SimpleCache.prototype.get = function (key, defaultValue) {
    const returnDefault = () => {
        if (typeof defaultValue === "function") {
            return defaultValue();
        }

        return defaultValue
    };

    if (!this.has(key)) {
        return returnDefault();
    }

    const storedObject = this.currentBucket[key];
    if (isExpired(storedObject)) {
        return returnDefault();
    }

    if (typeof storedObject.value === "object") {
        return Object.assign({}, storedObject.value);
    }

    return storedObject.value;
};

SimpleCache.prototype.set = function (key, value) {
    this.currentBucket[key] = value;
    this.autosave && this._save();

    return this;
};

SimpleCache.prototype.setBucket = function (bucket) {
    if (!this.buckets.hasOwnProperty(bucket)) {
        this.buckets[bucket] = {};
    }

    this.currentBucket = this.buckets[bucket];
    return this;
};

SimpleCache.prototype.removeBucket = function (bucket) {
    if (this.buckets.hasOwnProperty(bucket)) {
        delete this.buckets[bucket];
        this.autosave && this._save();
    }
    return this;
};

export default SimpleCache;
