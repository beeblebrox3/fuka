import App from "app";
const React = App.libs.React;

function Application(props) {
    return (
        <div>
            { props.children }
        </div>
    );
}

Application.displayName = "Application";
export default Application;
