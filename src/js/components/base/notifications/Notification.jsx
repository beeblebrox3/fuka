import App from "app";
const React = App.libs.React;

const Notification = function (props) {
    return (
        <div className={ "system-notification " + props.type }>
            <span className="system-notification-text">
                { props.message }
            </span>
            <span className="system-notification-dismiss-container">
                <span className="system-notification-dismiss"
                    onClick={ props.dismiss }
                >
                    Ok
                </span>
            </span>
        </div>
    );
};

Notification.defaultProps = {
    type: "info",
    message: "notification...",
    dismiss: null
};

export default Notification;