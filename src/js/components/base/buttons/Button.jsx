import App from "app";
const React = App.libs.React;

const Button = function (props) {
    "use strict";

    let className = props.className;
    if (props.large === true) {
        className += " btn-large";
    }

    if (props.primary) {
        className += " btn-primary";
    }

    let icon = "";
    let iconLeft = "";
    let iconRight = "";
    if (props.icon) {
        icon = <span className={ "icon app-" + props.icon + " icon-" + props["icon-side"] } />
        if (props["icon-side"] === "left") {
            iconLeft = icon;
        } else {
            iconRight = icon;
        }
    }

    if (props.href) {
        return (
            <a { ...props } className={ className }>
                { iconLeft }
                { props.children }
                { iconRight }
            </a>
        );
    } else if (props.tag === "span") {
        return (
            <span className={ className } onClick={ props.onClick }>
                { iconLeft }
                { props.children }
                { iconRight }
            </span>
        );
    }

    return (
        <button className={ className } onClick={ props.onClick }>
            { iconLeft }
            { props.children }
            { iconRight }
        </button>
    );
};

Button.defaultProps = {
    className: "btn",
    onClick: false,
    href: false,
    large: false,
    icon: false,
    "icon-side": "right",
    tag: "button",
    primary: false
};

export default Button;
