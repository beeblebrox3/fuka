import App from "app";
const React = App.libs.React;

import UserBar from "./UserBar";

function Brand() {
    return (
        <div className="brand">
            <a href="/" title="Fuka - Home Page">
                <h1>Fuka</h1>
                <h2>A Dribbble Client</h2>
            </a>

            <UserBar />
        </div>
    );
}

Brand.displayName = "Brand";
export default Brand;
