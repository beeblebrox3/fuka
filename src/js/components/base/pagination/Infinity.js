import App from "app";

const React = App.libs.React;

function Infinity(props) {
    const classPrevios = ["paginator-link paginator-previous"];
    const classNext = ["paginator-link paginator-next"];

    if (!props.next.length) {
        classNext.push("disabled");
    }

    if (!props.previous.length) {
        classPrevios.push("disabled");
    }

    return (
        <div className="paginator infinity-paginator">
            <a href={ props.previous }
               className={ classPrevios.join(" ") }
            >
                Previous
            </a>
            <a href={ props.next }
               className={ classNext.join(" ") }
            >
                Next
            </a>
        </div>
    );
}

Infinity.defaultProps = {
    next: "",
    previous: ""
};

Infinity.displayName = "Infinity";
export default Infinity;
