import App from "app";
const React = App.libs.React;

const DribbbleProvider = App.service("DribbbleProvider");

class UserBar extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            user: DribbbleProvider.getLoggedUser()
        };
    }

    _(statePath, defaultValue) {
        return App.helpers.object.getFlattened(statePath, this.state, defaultValue || "");
    }

    componentDidMount() {
        this.unsubscribe = App.EventManager.subscribe("DribbbleProvider.user_authenticated", (data) => {
            this.setState({user: data.user});
        });
    }

    componentWillUnmount() {
        this.unsubscribe();
    }

    renderLoginButton() {
        if (!this.state.loading && !this._("user.name", false)) {
            return (
                <span className="login">
                    <a href={ DribbbleProvider.getLoginUri() } className="login-button">Login</a>
                </span>
            );
        }
    }

    renderUserName() {
        if (!this.state.loading && this._("user.name", null) !== null) {
            return (
                <span className="user">
                    Hello, <a href={ this._("user.html_url") } title="visit your profile">{ this._("user.username") }</a>!
                </span>
            );
        }
    }

    renderLoading() {
        if (this.state.loading) {
            return "loading...";
        }
    }

    render() {
        let classes = ["userbar"];
        if (this.state.loading) {
            classes.push("loading");
        }

        if (this.state.user !== null) {
            classes.push("logged-in");
        }

        return (
            <div className={ classes.join(" ") }>
                { this.renderLoading() }
                { this.renderLoginButton() }
                { this.renderUserName() }
            </div>
        );
    }
}

UserBar.displayName = "UserBar";
export default UserBar;
