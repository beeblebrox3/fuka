import App from "app";
const React = App.libs.React;

function Loading() {
    return (
        <div className="loading-container">
            <span className="spinner" />
        </div>
    );
}

Loading.displayName = "Loading";
export default Loading;
