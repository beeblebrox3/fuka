import App from "app";
import Infinity from "./pagination/Infinity";
import UserBar from "./UserBar";
import Brand from "./Brand";
import Loading from "./Loading";
import Page from "./Page";

App.components.base = {};

App.components.base.Page = Page;
App.components.base.Infinity = Infinity;
App.components.base.UserBar = UserBar;
App.components.base.Brand = Brand;
App.components.base.Loading = Loading;
