import App from "app";
const React = App.libs.React;

function Page(props) {
    const classes = ["page"];
    if (props.name) {
        classes.push(props.name);
    }

    if (props.loading) {
        classes.push("loading");
    }

    return (
        <div className={ classes.join(" ") }>
            { props.children }
        </div>
    )
}

Page.displayName = "Page";
export default Page;
