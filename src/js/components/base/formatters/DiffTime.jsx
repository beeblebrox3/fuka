import App from "app";
const React = App.libs.React;
const beautifySeconds = App.helpers.date.beautifySeconds;

const DiffTime = function (props) {
    const secondsDiff = Math.abs(props.current - props.start) / 1000;

    if (props.start && props.current) {
        return (
            <span className="time-display">
                <span className="time">
                    { beautifySeconds(secondsDiff) }
                </span>
            </span>
        );
    }

    return (
        <span className="time-display">-</span>
    );
};

DiffTime.defaultProps = {
    start: 0,
    current: 0
};

export default DiffTime;
