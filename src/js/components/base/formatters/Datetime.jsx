import App from "app";
const React = App.libs.React;

const Datetime = function (props) {
    let dObject;
    let moment;
    let elem;

    if (!isNaN(props.dateTime) || props.dateTime.length) {
        dObject = new Date(props.dateTime);
        moment = {
            year: dObject.getFullYear(),
            month: dObject.getUTCMonth() + 1,
            day: dObject.getDate(),
            hours: dObject.getHours(),
            minutes: dObject.getMinutes()
        };

        elem = (
            <span className="time-display">
                <span className="date">
                    { ("0" + moment.day).slice(-2) }/
                    { ("0" + moment.month).slice(-2) }/
                    { moment.year }
                </span>
                <span className="time">
                    { ("0" + moment.hours).slice(-2) }:
                    { ("0" + moment.minutes).slice(-2) } h
                </span>
            </span>
        );
    } else {
        elem = (
            <span className="time-display"></span>
        );
    }

    return elem;
};

Datetime.defaultProps = {
    dateTime: ""
};

export default Datetime;
