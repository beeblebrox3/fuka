import App from "app";
const React = App.libs.React;

function NotFound() {
    return (
        <div className="container">
            <h1>Ops! Page not found!</h1>

            <p>Are you lost?</p>
        </div>
    );
}

NotFound.displayName = "NotFound";
export default NotFound;
