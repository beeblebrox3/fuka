import App from "app";
const React = App.libs.React;

const WallElement = function (props) {
    let classes = ["shots-wall-list-item", props.size];
    if (props.shot.animated) {
        classes.push("animated");
    }

    return (
        <li className={ classes.join(" ") }>
            <a href={ `/shots/${props.shot.id}` }>
                <img src={ props.shot.images.normal } alt="shot image" title={ props.shot.title } />

                <span className="shot-cover">
                    <span className="title">{ props.shot.title }</span>
                </span>
            </a>
        </li>
    );
};

WallElement.displayName = "WallElement";
export default WallElement;
