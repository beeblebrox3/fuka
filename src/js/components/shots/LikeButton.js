import App from "app";

const React = App.libs.React;
const DribbbleProvider = App.service("DribbbleProvider");

class LikeButton extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);

        this.state = {
            loading: true,
            liked: false
        };
    }

    componentDidMount() {
        DribbbleProvider.userDidLike(this.props.shotId)
            .then((liked) => {
                this.setState({liked: liked, loading: false})
            });
    }

    getText() {
        if (this.state.loading) {
            return "loading...";
        }

        if (this.state.liked) {
            return "liked";
        }

        return "like";
    }

    handleClick() {
        const changeLikeStatus = () => {
            const method = this.state.liked ? "dislike" : "like";
            DribbbleProvider[method](this.props.shotId).then(() => {
                this.setState({loading: false, liked: !this.state.liked})
            });
        };

        this.setState({loading: true}, changeLikeStatus);
    }

    render() {
        const classes = ["button like-button"];
        if (this.state.loading) {
            classes.push("loading");
        }

        if (this.state.liked) {
            classes.push("liked");
        }
        return (
            <button className={ classes.join(" ") } onClick={ this.handleClick }>
                ♥ { this.getText() }
            </button>
        );
    }
};

LikeButton.displayName = "LikeButton";
export default LikeButton;
