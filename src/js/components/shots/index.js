import App from "app";
import Wall from "./Wall";
import Shot from "./Shot";
import LikeButton from "./LikeButton";

App.components.shots = {};
App.components.shots.Wall = Wall;
App.components.shots.Shot = Shot;
App.components.shots.LikeButton = LikeButton;
