import App from "app";
const React = App.libs.React;

const Shot = function (props) {
    document.title = props.shot.title;

    return (
        <article className="shot">
            <h1>{ props.shot.title }</h1>

            <section className="shot-body">
                <a href={ props.shot.html_url } title="" target="_blank">
                    <img src={ props.shot.images.hidpi } />
                </a>
            </section>

            <section className="shot-author">
                <a href={ props.shot.user.html_url }
                    title={ props.shot.user.name }
                    target="_blank"
                >
                    <img src={ props.shot.user.avatar_url } alt="photo of the author" />

                </a>

                by { " " }
                <a href={ props.shot.user.html_url }
                    title={ props.shot.user.name }
                    target="_blank"
                >
                    @{ props.shot.user.username }

                </a>
            </section>

            <section className="shot-statistics">
                <ul className="shot-statistics-list">
                    <li className="shot-statistcs-info">{ props.shot.views_count } views</li>
                    <li className="shot-statistcs-info">{ props.shot.likes_count } likes</li>
                    <li className="shot-statistcs-info">{ props.shot.comments_count } comments</li>
                </ul>
            </section>

            <section className="shot-description"
                dangerouslySetInnerHTML={ {__html: props.shot.description ? props.shot.description : "-- no description provided --" } }
            >
            </section>
        </article>
    );
}

Shot.displayName = "Shot";
export default Shot;
