import App from "app";
import WallElement from "./WallElement";

const React = App.libs.React;

const Wall = function (props) {
    let classes = ["shots-wall"];
    if (props.loading) {
        classes.push("loading");
    }

    function renderNothingFound() {
        if (props.showEmptyMessage === true && !props.shots.length) {
            return "Ops! There's nothing here!";
        }
    }

    return (
        <div className={ classes.join(" ") }>
            { renderNothingFound() }

            <ul className="shots-wall-list">
                { props.shots.map(shot => <WallElement key={ shot.id } shot={ shot } size={ props.thumbnailSize }/>) }
            </ul>
        </div>
    );
};

Wall.defaultProps = {
    shots: [],
    thumbnailSize: "medium"
};

Wall.displayName = "Wall";
export default Wall;
