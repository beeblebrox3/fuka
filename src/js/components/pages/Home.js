import App from "app";

const React = App.libs.React;
const qs = App.libs.qs;
const Wall = App.components.shots.Wall;
const DribbbleProvider = App.service("DribbbleProvider");

class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shots: [],
            loading: true
        };

        this.handleSearch = this.handleSearch.bind(this);
    }

    componentDidMount() {
        this.loadPage(this.props);
    }

    componentWillReceiveProps(newProps) {
        const shouldUpdate = ["page", "per_page", "q", "thumbnail"].some(prop => {
            return newProps[prop] != this.props[prop];
        });

        if (shouldUpdate) {
            window.scrollTo(0,0);
            this.loadPage(newProps);
        }
    }

    loadPage(props) {
        const opts = {page: props.page, per_page: props.per_page,  q: props.q};

        this.setState({loading: true}, () => {
            DribbbleProvider.shots(opts)
                .then(shots => {
                    this.setState({loading: false, shots: shots})
                });
        });
    }

    handleSearch() {
        App.service("ROUTER").setRoute(this.getUrl(null, null, null, this.inputSearch.value));
    }

    getSearchValue() {
        if (this.inputSearch && this.inputSearch.value) {
            return this.inputSearch.value;
        }

        return "";
    }

    getShots() {
        const q = this.getSearchValue();
        if (!q.length) {
            return this.state.shots;
        }

        return this.state.shots.filter((shot) => {
            return shot.title.includes(q);
        });
    }

    getUrl(page, per_page, q, thumbnail) {
        const query = {
            page: page || this.props.page,
            per_page: per_page || this.props.per_page,
            q: q || this.props.q,
            thumbnail: thumbnail || this.props.thumbnail
        };
        return `/?${qs.stringify(query)}`;
    }

    getNextLink() {
        if (this.state.shots.length < this.props.per_page) {
            return "";
        }

        return this.getUrl(parseInt(this.props.page, 10) + 1);
    }

    getPreviousLink() {
        const page = parseInt(this.props.page, 10) || 1;
        if (page === 1) {
            return "";
        }

        return this.getUrl(page - 1);
    }

    getThumbnailLink(size) {
        return this.getUrl(null, null, null, size);
    }

    renderLoading() {
        if (this.state.loading) {
            return <App.components.base.Loading />;
        }
    }

    renderThumbnailSizeButton(size) {
        return (
            <a className={ `button mini ${size} ${size === this.props.thumbnail ? "active" : ""}` }
               href={ this.getThumbnailLink(size) }
            >
                { size }
            </a>
        );
    }

    render() {
        return (
            <App.components.base.Page name="home" loading={ this.state.loading }>
                <App.components.base.Brand />

                <div className="page-body">
                    <div className="search-bar">
                        <input type="search"
                               placeholder="Search (by title, on this page)..."
                               defaultValue={ this.props.context.query.q }
                               onChange={ App.libs.debounce(this.handleSearch, 500) }
                               ref={ (elem) => { this.inputSearch = elem }}
                        />
                    </div>

                    <div className="sizes-selector">
                        <ul className="inline-list">
                            { ["small", "medium", "large"].map(size => {
                                return <li key={ size }>{ this.renderThumbnailSizeButton(size) }</li>;
                            }) }
                        </ul>
                    </div>

                    <Wall shots={ this.getShots() }
                          showEmptyMessage={ !this.state.loading }
                          thumbnailSize={ this.props.thumbnail }
                    />

                    <App.components.base.Infinity
                        previous={ this.getPreviousLink() }
                        next={ this.getNextLink() }
                    />

                    { this.renderLoading() }
                </div>
            </App.components.base.Page>
        );
    }
}

Home.defaultProps = {
    page: 1,
    per_page: 30,
    q: "",
    thumbnail: "medium"
};

Home.displayName = "Home";
export default Home;
