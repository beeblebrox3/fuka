import App from "app";
import Home from "./Home";
import Shot from "./Shot";
import Working from "./Working";

App.components.pages = {};

App.components.pages.Home = Home;
App.components.pages.Shot = Shot;
App.components.pages.Working = Working;
