import App from "app";

const React = App.libs.React;
const DribbbleProvider = App.service("DribbbleProvider");

class Shot extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            shot: {},
            loading: true
        };
    }

    componentDidMount() {
        DribbbleProvider
            .shot(this.props.shotId)
            .then(shot => this.setState({shot: shot, loading: false}));
    }

    _(statePath, defaultValue) {
        return App.helpers.object.getFlattened(statePath, this.state, defaultValue || "");
    }

    renderShot() {
        if (!this.state.loading) {
            return (
                <article className={ `shot ${this.state.loading ? "loading" : "" }` }>
                    <h1>{ this._("shot.title") }</h1>

                    <App.components.shots.LikeButton
                        shotId={ this.props.shotId }
                    />

                    <section className="shot-body">
                        <a href={ this._("shot.html_url") } title="" target="_blank">
                            <img src={ this._("shot.images.hidpi") || this._("shot.images.normal") }/>
                        </a>
                    </section>

                    <section className="shot-author">
                        <a href={ this._("shot.html_url") }
                           title={ this._("shot.user.name") }
                           target="_blank"
                        >
                            <img src={ this._("shot.user.avatar_url") }
                                 alt="photo of the author"
                            />
                        </a>

                        by { " " }
                        <a href={ this._("shot.user.html_url") }
                           title={ this._("shot.user.name") }
                           target="_blank"
                        >
                            @{ this._("shot.user.username") }
                        </a>
                    </section>

                    <section className="shot-statistics">
                        <ul className="shot-statistics-list">
                            <li className="shot-statistcs-info">{ this._("shot.views_count") } views</li>
                            <li className="shot-statistcs-info">{ this._("shot.likes_count") } likes</li>
                            <li className="shot-statistcs-info">{ this._("shot.comments_count") } comments</li>
                        </ul>
                    </section>

                    <section className="shot-description"
                             dangerouslySetInnerHTML={ {__html: this._("shot.description", "-- no description provided --")} }
                    >
                    </section>
                </article>
            );
        }
    }

    renderLoading() {
        if (this.state.loading) {
            return <App.components.base.Loading />;
        }
    }

    render() {
        if (!this.loading) {
            document.title = `${this._("shot.title", "")} | Fuka`;
        }

        return (
            <App.components.base.Page name="home" loading={ this.state.loading }>
                <App.components.base.Brand />

                <div className="page-body">
                    { this.renderShot() }
                    { this.renderLoading() }
                </div>
            </App.components.base.Page>
        );
    }
}

Shot.displayName = "Shot";
export default Shot;
