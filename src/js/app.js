const App = window.App || {};

App.components = App.components || {};
App.libs = App.libs || {};
App.helpers = App.helpers || {};
App.services = App.services || {};

export default App;
