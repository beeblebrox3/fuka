import App from "app";

const md5 = App.libs.md5;

function DribbbleProvider() {
    this.providerName = "DribbbleProvider";
    this.backendBasepath = App.Config.get("backendPath");
    this.appBasepath = App.Config.get("basepath");

    this.Request = App.service("AJAX");
    this.EM = App.EventManager;
    this.cache = App.service("Cache");

    this.rateLimit = 0;
    this.rateLimitRamaining = 0;

    this.clientId = App.Config.get("dribbbleClientId");
    this.clientSecret = App.Config.get("dribbbleClientSecret");
    this.accessToken = App.Config.get("dribbbleAccessToken");

    this.userAccessToken = "";
    this.userAccessTokenType = "";
    this.userScope = "";
    this.user = null;

    this.loadFromLocalStorage();
}

DribbbleProvider.prototype.loadFromLocalStorage = function () {
    let data = localStorage.getItem("fuka_tokens");
    if (!data) {
        return;
    }

    try {
        data = JSON.parse(data);
        ["userAccessToken", "userAccessTokenType", "userScope", "user"].map(info => {
            if (data.hasOwnProperty(info)) {
                this[info] = data[info];
            }
        });
    } catch (e) {
        console.log("invalid state: ");
        console.log(data);
    }
};

DribbbleProvider.prototype.saveOnLocalStorage = function () {
    localStorage.setItem("fuka_tokens", JSON.stringify({
        userAccessToken: this.userAccessToken,
        userAccessTokenType: this.userAccessTokenType,
        userScope: this.userScope,
        user: this.user
    }));
};

DribbbleProvider.prototype.notify = function (eventName, args) {
    this.EM.notify(`${this.providerName}.${eventName}`, args);
};

DribbbleProvider.prototype.getLoggedUser = function () {
    return this.user;
};

DribbbleProvider.prototype.updateRateLimitInfo = function (limit, remaining) {
    this.rateLimit = parseInt(limit);
    this.rateLimitRamaining = parseInt(remaining);

    this.notify("rate_limit_updated", {limit: this.rateLimit, remaining: this.rateLimitRamaining});
};

DribbbleProvider.prototype.request = function (method, url, args, type, skipCache) {
    return new Promise((resolve, reject) => {
        const payload = Object.assign({}, args, {access_token: this.userAccessToken || this.accessToken});
        const hash = `dribbble-${md5(JSON.stringify({url, payload, method}))}`;

        if (skipCache !== true && method === "get" && this.cache.has(hash, true)) {
            resolve(this.cache.get(hash));
            return;
        }

        this.Request.make({
            url: App.helpers.string.resolveUrl(url, this.backendBasepath),
            method: method,
            onStop: (err, res) => {
                if (err) return;
                this.updateRateLimitInfo(res.header['X-RateLimit-Limit'], res.header['X-RateLimit-Remaining']);
            },
            onSuccess: res => {
                this.cache.set(hash, res.body, 600);
                resolve(res.body)
            },
            type: type || "json",
            onError: err => reject(err),
            data: payload || {}
        });
    });
};

DribbbleProvider.prototype.getLoginUri = function () {
    const qs = App.libs.qs.stringify({
        client_id: this.clientId,
        scope: "public write",
        state: Math.random(),
        redirect_uri: this.appBasepath + "auth"
    });
    return `https://dribbble.com/oauth/authorize?${qs}`;
};

DribbbleProvider.prototype.auth = function (webtoken) {
    return new Promise((resolve, reject) => {
        const payload = {
            client_id: this.clientId,
            client_secret: this.clientSecret,
            code: webtoken
        };

        this.request("post", "oauth/token", payload, "form")
            .then((res) => {
                this.userAccessToken = res.access_token;
                this.userAccessTokenType = res.token_type;
                this.userScope = res.scope;

                this.request("get", "user", {})
                    .then(res => {
                        this.user = res;
                        this.saveOnLocalStorage();

                        this.notify("user_authenticated", {user: res});

                        resolve(true);
                    });
            })
            .catch((err) => {
                reject(err);
            });
    });
};

DribbbleProvider.prototype.shots = function (args) {
    return this.request("get", "shots", args);
};

DribbbleProvider.prototype.shot = function (shotId) {
    return this.request("get", `shots/${shotId}`);
};

DribbbleProvider.prototype.like = function (shotId) {
    return this.request("post", `shots/${shotId}/like`);
};

DribbbleProvider.prototype.dislike = function (shotId) {
    return this.request("delete", `shots/${shotId}/like`);
};


DribbbleProvider.prototype.userDidLike = function (shotId) {
    return new Promise((resolve, reject) => {
        this.request("get", `shots/${shotId}/like`, {}, "json", true)
            .then(() => resolve(true))
            .catch(err => {
                if (err.status === 404) {
                    resolve(false);
                    return;
                }
                reject(err);
            });
    });
};

export default DribbbleProvider;
