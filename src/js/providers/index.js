import App from "app";
import DribbbleProvider from "./Dribbble";

App.ServicesContainer.define("DribbbleProvider", DribbbleProvider);
