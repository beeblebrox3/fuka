import App from "app";

import ArrayHelpers from "./array/index";
import DateHelpers from "./date/index";
import StringHelpers from "./string/index";
import ObjectHelpers from "./object/index";

App.helpers.array = ArrayHelpers;
App.helpers.date = DateHelpers;
App.helpers.string = StringHelpers;
App.helpers.object = ObjectHelpers;
