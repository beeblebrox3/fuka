<?php

require "vendor/autoload.php";

ini_set("display_errors", "on");
ini_set("error_reporting", E_ALL);

use Proxy\Proxy;
use Proxy\Adapter\Guzzle\GuzzleAdapter;
use Proxy\Filter\RemoveEncodingFilter;
use Zend\Diactoros\ServerRequestFactory;

// Create a PSR7 request based on the current browser request.
$request = ServerRequestFactory::fromGlobals();

// Create a guzzle client
$guzzle = new GuzzleHttp\Client();

// Create the proxy instance
$proxy = new Proxy(new GuzzleAdapter($guzzle));

// Add a response filter that removes the encoding headers.
$proxy->filter(new RemoveEncodingFilter());

$proxy->filter(function (\Zend\Diactoros\ServerRequest $request, \Zend\Diactoros\Response $response, $next) {
    $uri = $request->getUri();

    if (strpos($uri->getPath(), "oauth/token") !== false) {
        $uri = $uri->withHost("dribbble.com")->withPath("/oauth/token");
    }

    $request = $request->withUri(
        $uri->withPort(443)
    );

    $response = $next($request, $response);

    if (!$response->hasHeader("Access-Control-Allow-Origin")) {
        $response = $response->withAddedHeader('Access-Control-Allow-Origin', '*')
            ->withAddedHeader('Access-Control-Allow-Headers', 'Content-Type');
    }
    return $response;
});

// Forward the request and get the response.
try {
    $response = $proxy->forward($request)->to('https://api.dribbble.com/v1/');
} catch (\GuzzleHttp\Exception\ClientException $e) {
    $response = $e->getResponse();
}


// Output response to the browser.
(new Zend\Diactoros\Response\SapiEmitter)->emit($response);
